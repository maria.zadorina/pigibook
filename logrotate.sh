#!/usr/bin/env bash

if [ ! -d '/var/log/pigibook' ]; then
    mkdir /var/log/pigibook
    chown root /var/log/pigibook
fi

if [ ! -f '/etc/logrotate.d/pigibook' ]; then
    echo "/var/log/pigibook/*.log {
        su root root
        daily
        missingok
        rotate 5
        copytruncate
        compress
        delaycompress
        size 2M
    }" >> /etc/logrotate.d/pigibook
fi

logrotate -fv /etc/logrotate.d/pigibook
