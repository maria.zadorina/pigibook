#!/bin/bash
# инициализация стартовых настроек при развёртывании нового проекта.
# перед запуском скрипта установить права на выполнение. chmod ugo+x init.sh
# запуск: ./init.sh

sudo chmod +x ./logrotate.sh
sudo chmod +x ./manage.py
sudo ./logrotate.sh
sudo ln -s /home/web/pigibook/conf.d/nginx/pigibook.web-dev.conf /etc/nginx/conf.d/
sudo nginx -s reload

docker-compose up -d --build
docker-compose ps
docker-compose exec app python manage.py migrate
docker-compose exec app python manage.py collectstatic
docker-compose exec app python manage.py add_test_data
