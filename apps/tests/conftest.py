from uuid import uuid4

import pytest

from apps.location.models import Locality
from apps.pig.models import UnitPig


@pytest.fixture
def unit_pig_data():
    return {
        "type": "SOW",
        "uid": "DORY124",
        "name": "DORY124",
        "birthday": "2020-01-01",
        "breeds": {
            "me": {
                "name": "Северокавказская",
                "uid": "123456789",
            },
            "mom": {
                "name": "Эстонская беконная",
                "uid": "223456789",
            },
            "dad": {
                "name": "Mиргородская",
                "uid": "323456789",
            },
        },
        "sector_id": 1,
    }


@pytest.fixture
def pig_data():
    return {"pig_id": "DORY124"}


@pytest.fixture
def sow_data():
    return {"sow_id": "DORY124"}


@pytest.fixture
def create_unitpig(unit_pig_data):
    return UnitPig.objects.create(**unit_pig_data)


@pytest.fixture
def localty_data():
    return {
        "name": "Дороничи, Участок 1",
        "location_type": "DISTRICT",
        "period": 100,
    }


@pytest.fixture
def create_location(localty_data):
    return Locality.objects.create(**localty_data)


@pytest.fixture
def action_data():
    return {
        "action_type": "INSEMINATION",
        "unit_pig_id": "DORY124",
        "event": "OTHER",
        "weight": 100,
        "sector_id": 1,
        "comment": "DORY124",
        "info": {"batch_semen_id": "Партия 1"},
        "meta": {"inside_womb": True, "count_doses": 2, "artificial": True},
    }
