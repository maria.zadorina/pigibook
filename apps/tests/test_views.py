from uuid import uuid4

import pytest
from django import urls

URL = urls.reverse("api")


def jsonrpc_data(method, params):
    return {
        "id": str(uuid4()),
        "jsonrpc": "2.0",
        "method": method,
        "params": params,
    }


@pytest.mark.django_db
def test_create_unitpig(client, unit_pig_data):
    response = client.post(
        URL,
        jsonrpc_data("create_unitpig", unit_pig_data),
        content_type="application/json",
    ).json()
    assert response.get("error") is None, f"Error {response.get('error')}"
    if response.get("result"):
        print(response.get("result"))


@pytest.mark.django_db
def test_get_pig(client, pig_data):
    response = client.post(
        URL,
        jsonrpc_data("get_pig", pig_data),
        content_type="application/json",
    ).json()
    assert response.get("error") is None, f"Error {response.get('error')}"
    if response.get("result"):
        print(response.get("result"))


@pytest.mark.django_db
def test_get_pig_location(client, pig_data):
    response = client.post(
        URL,
        jsonrpc_data("get_pig_location", pig_data),
        content_type="application/json",
    ).json()
    assert response.get("error") is None, f"Error {response.get('error')}"
    if response.get("result"):
        print(response.get("result"))


@pytest.mark.django_db
def test_create_location(client, localty_data):
    response = client.post(
        URL,
        jsonrpc_data("create_location", localty_data),
        content_type="application/json",
    ).json()
    assert response.get("error") is None, f"Error {response.get('error')}"
    if response.get("result"):
        print(response.get("result"))


@pytest.mark.django_db
def test_add_action(
    client,
    create_location,
    create_unitpig,
    action_data,
):
    response = client.post(
        URL,
        jsonrpc_data("add_or_update_action_in_event_log", action_data),
        content_type="application/json",
    ).json()
    assert response.get("error") is None, f"Error {response.get('error')}"
    if response.get("result"):
        print(response.get("result"))


@pytest.mark.django_db
def test_get_history_changes(client, sow_data):
    response = client.post(
        URL,
        jsonrpc_data("get_history_changes", sow_data),
        content_type="application/json",
    ).json()
    assert response.get("error") is None, f"Error {response.get('error')}"
    if response.get("result"):
        print(response.get("result"))


@pytest.mark.django_db
def test_get_insemination_history(client, sow_data):
    response = client.post(
        URL,
        jsonrpc_data("get_insemination_history", sow_data),
        content_type="application/json",
    ).json()
    assert response.get("error") is None, f"Error {response.get('error')}"
    if response.get("result"):
        print(response.get("result"))


@pytest.mark.django_db
def test_get_sow_card(client, create_location, create_unitpig, sow_data):
    response = client.post(
        URL,
        jsonrpc_data("get_sow_card", sow_data),
        content_type="application/json",
    ).json()
    assert response.get("error") is None, f"Error {response.get('error')}"
    if response.get("result"):
        print(response.get("result"))
