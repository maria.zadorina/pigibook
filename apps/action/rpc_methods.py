import logging

from jsonrpc.decorators import remote_procedure
from jsonrpc.exceptions import InvalidParams

from apps.action.models import EventLog
from apps.insemination.models import InseminationJournal
from apps.location.models import Locality
from apps.location.models import Partner
from apps.pig.models import UnitPig

log = logging.getLogger(__name__)


def check_info(action_type: str, info: dict) -> bool:
    if action_type == "INSEMINATION":
        try:
            InseminationJournal.objects.get(batch_semen_id=info.get("batch_semen_id"))
            return True
        except InseminationJournal.DoesNotExist:
            # todo: raise think about it
            return False
    if action_type == "FARROW":
        return True
    if action_type == "DIRECTION":
        return True
    if action_type == "ENTRANCE":
        return True
    if action_type == "TRANSFER_IN" or action_type == "TRANSFER_OUT":
        return True
    if action_type == "WEIGHING":
        return True
    if action_type == "TRANSPLANT_IN" or action_type == "TRANSPLANT_OUT":
        return True


@remote_procedure
def add_or_update_action_in_event_log(
    action_type: str,
    unit_pig_id: int,
    event: str = "OTHER",
    weight: int = None,
    sector_id: int = None,
    info: dict = None,
    meta: dict = None,
    comment: str = None,
    partner_id: int = None,
    event_log_id: int = None,
) -> dict:
    # todo: auth
    # todo: What to do with unitpig, that remove? add active flag? or transfer other table?
    unit_pig, sector, partner = None, None, None
    try:
        unit_pig = UnitPig.objects.get(uid=unit_pig_id)
        if sector_id:
            sector = Locality.objects.get(id=sector_id)
        if partner_id:
            partner = Partner.objects.get(id=partner_id)
    except UnitPig.DoesNotExist:
        log.error(f"UnitPig {unit_pig_id} does not exist")
        raise InvalidParams(f"UnitPig {unit_pig_id} does not exist")
    except Locality.DoesNotExist:
        log.error(f"Locality {sector_id} does not exist")
    except Partner.DoesNotExist:
        log.error(f"Partner {partner_id} does not exist")

    data = {
        "action_type": action_type,
        "unit_pig": unit_pig,
        "event": event,
        "weight": weight,
        "sector": sector,
        "info": info if check_info(action_type, info) else {},
        "meta": meta,
        "comment": comment,
        "partner": partner,
    }
    if event_log_id:
        EventLog.objects.filter(id=event_log_id).update(**data)
        created = False
    else:
        EventLog.objects.create(**data)
        created = True
    return {"created": created}
