from django.apps import AppConfig


class ActionConfig(AppConfig):
    name = "apps.action"
    verbose_name = "Действия над свиньями"
