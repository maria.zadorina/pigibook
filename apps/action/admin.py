from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from .models import EventLog


@admin.register(EventLog)
class EventLogAdmin(admin.ModelAdmin):
    list_display = (
        "action_type",
        "event",
        "weight",
        "unit_pig_link",
        "sector_link",
        "partner_link",
        "responsible_link",
        "updated",
        "created",
    )
    list_filter = ("action_type", "event", "sector", "partner", "updated", "created")
    list_select_related = ("unit_pig", "sector", "partner", "responsible")
    raw_id_fields = ("unit_pig", "sector", "partner", "responsible")
    readonly_fields = ("info", "meta", "updated", "created")
    show_full_result_count = False

    # todo: search by uniq_pig_uid

    def unit_pig_link(self, obj):
        if obj.unit_pig is None:
            return format_html('<a href="#">none</a>')
        link = reverse("admin:pig_unitpig_change", args=[obj.unit_pig_id])
        return format_html('<a href="{}">{}</a>', link, obj.unit_pig.uid)

    unit_pig_link.short_description = "Свино-единица"

    def sector_link(self, obj):
        if obj.sector is None:
            return format_html('<a href="#">none</a>')
        link = reverse("admin:location_locality_change", args=[obj.sector_id])
        return format_html('<a href="{}">{}</a>', link, obj.sector.name)

    sector_link.short_description = "Сектор"

    def partner_link(self, obj):
        if obj.partner is None:
            return format_html('<a href="#">none</a>')
        link = reverse("admin:location_partner_change", args=[obj.partner_id])
        return format_html('<a href="{}">{}</a>', link, obj.partner.name)

    partner_link.short_description = "Поставщик/Получатель"

    def responsible_link(self, obj):
        if obj.responsible is None:
            return format_html('<a href="#">none</a>')
        link = reverse("admin:core_user_change", args=[obj.responsible_id])
        return format_html(
            '<a href="{}">{}</a>',
            link,
            f"{obj.responsible.last_name} {obj.responsible.first_name}",
        )

    responsible_link.short_description = "Ответственный"
