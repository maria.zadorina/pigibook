from datetime import timedelta

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from jsonfield import JSONField

from apps.insemination.models import InseminationJournal
from apps.pig.models import UnitPig


class StateEntrance(models.TextChoices):
    # Состояние Поступление - Entrance
    NONE = "NONE", "Неопределено"
    SINGLE = "SINGLE", "Холостая"
    SUCKLING = "SUCKLING", "Подсосная"  # кормящая
    PREGNANT = "PREGNANT", "Супоросная"
    CONDITIONALLY_PREGNANT = "CONDITIONALLY_PREGNANT", "Условно-супоросная"


class ActionType(models.TextChoices):
    # Вид действия
    NONE = "NONE", "Не определено"
    DIRECTION = "DIRECTION", "Выбытие"
    ENTRANCE = "ENTRANCE", "Поступление"
    FARROW = "FARROW", "Опорос"
    INSEMINATION = "INSEMINATION", "Осеменение"
    TRANSFER_IN = "TRANSFER_IN", "Перемещение в"
    TRANSFER_OUT = "TRANSFER_OUT", "Перемещение из"
    WEIGHING = "WEIGHING", "Взвешивание"
    TRANSPLANT_OUT = "TRANSPLANT_OUT", "Отсад"
    TRANSPLANT_IN = "TRANSPLANT_in", "Подсад"


class EventLog(models.Model):
    EVENTS_CHOICE = [
        (
            "DIRECTION",
            (
                ("REALIZE", "Реализовано"),
                ("TRANSFER", "Переведено на другие фермы"),
                ("DEATH", "Пало"),
                ("KILLING", "Забито"),
            ),
        ),
        (
            "ENTRANCE",
            (
                ("LITTER", "Приплод"),
                ("TRANSFER", "Переведено с других ферм"),
                ("BUY", "Куплено"),
            ),
        ),
        (
            "TRANSFER",
            (
                ("TRANSFER_GROUP", "Переведено с другой группы"),
                ("TRANSFER_CATEGORY", "Переведено из другой категории"),
                ("TRANSFER_SECTOR", "Переведено с другого участка"),
            ),
        ),
        ("OTHER", "Прочее"),
    ]
    event = models.CharField(
        "Событие",
        max_length=20,
        choices=EVENTS_CHOICE,
        default="OTHER",
        help_text="event | Выбор событий",
    )
    action_type = models.CharField(
        "Вид действия",
        max_length=15,
        default=ActionType.NONE,
        choices=ActionType.choices,
        help_text="action_type | Вид действия",
    )
    weight = models.PositiveSmallIntegerField(
        "Вес свиньи или группы",
        default=0,
        null=True,
        blank=True,
        help_text="weight | Вес свиньи или группы (зависит от того, над кем совершаем действие)",
    )
    unit_pig = models.ForeignKey(
        "pig.UnitPig",
        models.CASCADE,
        related_name="events",
        verbose_name="Свино-единица",
        help_text="unit_pig | Свино-единица",
    )
    sector = models.ForeignKey(
        "location.Locality",
        models.SET_NULL,
        null=True,
        blank=True,
        related_name="events",
        verbose_name="Сектор",
        help_text="sector | Сектор",
    )
    # fixme: есть ли закрепление за сектором его функциональности
    #  (что делают на секторе? только осеменяют или только опорос)
    partner = models.ForeignKey(
        "location.Partner",
        models.SET_NULL,
        null=True,
        blank=True,
        related_name="events",
        verbose_name="Поставщик/Получатель",
        help_text="partner | Поставщик/Получатель",
    )
    responsible = models.ForeignKey(
        "core.User",
        models.SET_NULL,
        null=True,
        blank=True,
        related_name="events",
        verbose_name="Ответственный",
        help_text="responsible | Ответственный",
    )
    info = JSONField(
        "Информация о действии",
        null=True,
        blank=True,
        default={},
        help_text="info | Информация о действии",
    )
    meta = JSONField(
        "Доп. информация о действии",
        null=True,
        blank=True,
        default={},
        help_text="meta | Доп. информация о действии",
    )
    comment = models.TextField(
        "Комментарий",
        null=True,
        blank=True,
        help_text="comment",
    )

    updated = models.DateTimeField("Обновлено", auto_now=True)
    created = models.DateTimeField("Создано", auto_now_add=True)

    class Meta:
        verbose_name = "Событие"
        verbose_name_plural = "Журнал событий"

    def __str__(self):
        return f"Действие вида {self.action_type}"

    def expected_farrow(self) -> str:
        # ожидаемый опрос
        # todo: надо забить константы в настройки,
        #  например, через сколько дней после осеменения будет опорос
        return (self.created + timedelta(days=150)).strftime("%d.%m.%Y")

    def insemination_week(self) -> int:
        # неделя осеменения
        now = timezone.now()
        days = (now - self.created).days
        if days > 7:
            return days // 7
        else:
            return 0

    # fixme meta - count weigth comment
    #  info - Unique information about action
    # help_text for meta and info:
    # 1. "DIRECTION", "Выбытие"
    # meta:
    #   {"culling": true, "reason_leaving": "id", "count": 1}
    #   culling - выбраковка; reason_leaving - причина выбытия Причина падежа / выбраковки
    # info: {}
    # _____
    # 2. "ENTRANCE", "Поступление"
    # meta:
    #   {"state": "Супоросная", "count": 1}
    #   state - Состояние (State)
    # info: {}
    # _____
    # 3. "FARROW", "Опорос"
    # meta:
    #   {"assistance": true, "nipples_count": 10, "socket_number": 1}
    # info:
    #   {"total_piglets": 10, "live_piglets": 10, "normal_piglets": 10,
    #   "weak_piglets": 0, "dead_piglets": 0, "mummy": 0, "boars": 5, "pigs": 5,
    #   "weight_group": 100}
    # Состояние вымени UdderState ("SATISFACTORY", "Удовлетворительно")
    # ______
    # 4. "INSEMINATION", "Осеменение"
    # meta:
    #   {"inside_womb": true, "count_doses": 2, "artificial": true}
    # info:
    #   {"batch_semen": "Партия семени"}
    # ______
    # 5. "TRANSFER_IN", "Пермещение в" "TRANSFER_OUT", "Пермещение из"
    # meta:
    #   {"state": "Состояние", "count": 1}
    # info: {}
    # ______
    # 6. "TRANSPLANT_OUT", "Отсад" "TRANSPLANT_in", "Подсад"
    # meta:
    #   {"socket_number": 1, "count": 1}
    # info: {}
    # ______
    # 7. "WEIGHING", "Взвешивания"
    # meta:
    #   {"fat": 10, "count": 1} fat - толщина шпика
    # info: {}


@receiver(post_save, sender=EventLog)
def update_unit_pig_after_action(sender, instance, created, **kwargs):
    """
    При создании СвиноЕдиницы создается событие поступление
    """
    data = {"sector": instance.sector}
    if created:
        if instance.action_type == ActionType.DIRECTION:
            data.update(implemented=True)
        UnitPig.objects.update(**data)
