from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class User(AbstractUser):
    pass

    def __str__(self):
        return f"{self.last_name} {self.first_name}"

    # snils = models.CharField("SNILS", max_length=14, blank=True, null=True)

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"


class Profile(models.Model):
    post = models.CharField("Должность", max_length=150, null=True, blank=True)
    e_mail = models.EmailField("e-mail", null=True, blank=True)
    fio = models.CharField(
        "Фамилия Имя Отчество",
        max_length=300,
        null=True,
        blank=True,
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    location = models.ForeignKey(
        "location.Locality",
        models.SET_NULL,
        null=True,
        blank=True,
        related_name="profile",
        verbose_name="Комплекс",
        help_text="location | Свинокомплекс",
    )

    class Meta:
        verbose_name = "Профиль"
        verbose_name_plural = "Профили"

    def __str__(self):
        return f"{self.user.username}, {self.fio} ({self.post})"


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    """
    При создании пользователя создается его профиль
    """
    if created:
        Profile.objects.create(user=instance)
