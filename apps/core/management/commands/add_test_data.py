from datetime import date
from datetime import datetime
from datetime import timedelta
from random import randint
from uuid import uuid4

from django.core.management.base import BaseCommand
from django.db.models import Q

from apps.action.models import EventLog
from apps.core.models import User
from apps.food.models import Nomenclature
from apps.food.models import NomenclatureType
from apps.food.models import TrafficType
from apps.food.models import Unit
from apps.insemination.models import InseminationJournal
from apps.location.models import Locality
from apps.location.models import LocalityWork
from apps.location.models import LocationType
from apps.location.models import Partner
from apps.location.models import ReceiptType
from apps.pig.models import Breed
from apps.pig.models import Group
from apps.pig.models import ReasonForLeaving
from apps.pig.models import UnitPig
from apps.pig.models import UnitPigType


class Command(BaseCommand):
    help = "Начальное заполнение базы данных"

    def handle(self, *args, **options):
        self.stdout.write(f"Старт инициализации базы данных;")
        models = (
            NomenclatureType,
            Nomenclature,
            InseminationJournal,
            Locality,
            LocalityWork,
            Partner,
            Breed,
            Group,
            ReasonForLeaving,
            UnitPig,
            User,
        )
        for model in models:
            print(model.objects.all().delete())

        User.objects.create_superuser(username="admin", password="admin")

        test_organization = Partner.objects.create(
            uid=str(uuid4()),
            name="Дороничи",
            inn="2234567890",
            receipt_type=ReceiptType.INTERNAL,
        )

        Locality.objects.create(
            uid=str(uuid4()),
            name="Русское",
            location_type=LocationType.PIG_COMPLEX,
            organization=test_organization,
        )
        Locality.objects.create(
            uid=str(uuid4()),
            name="Окуни",
            location_type=LocationType.PIG_COMPLEX,
            organization=test_organization,
        )
        Locality.objects.create(
            uid=str(uuid4()),
            name="Стрижи",
            location_type=LocationType.PIG_COMPLEX,
            organization=test_organization,
        )
        loc1 = Locality.objects.create(
            uid=str(uuid4()),
            name="Участок №1",
            period=randint(100, 150),
            location_type=LocationType.DISTRICT,
            organization=test_organization,
        )
        loc2 = Locality.objects.create(
            uid=str(uuid4()),
            name="Участок №2",
            period=randint(100, 150),
            location_type=LocationType.DISTRICT,
            organization=test_organization,
        )
        Locality.objects.create(
            uid=str(uuid4()),
            name="Участок №3",
            period=randint(100, 150),
            location_type=LocationType.DISTRICT,
            organization=test_organization,
        )
        sector1 = Locality.objects.create(
            uid=str(uuid4()),
            name="Сектор №1",
            parent=loc1,
            period=randint(100, 150),
            location_type=LocationType.SECTOR,
            organization=test_organization,
        )
        sector2 = Locality.objects.create(
            uid=str(uuid4()),
            name="Сектор №2",
            parent=loc1,
            period=randint(100, 150),
            location_type=LocationType.SECTOR,
            organization=test_organization,
        )
        sector3 = Locality.objects.create(
            uid=str(uuid4()),
            name="Сектор №3",
            parent=loc2,
            period=randint(100, 150),
            location_type=LocationType.SECTOR,
            organization=test_organization,
        )
        store = Locality.objects.create(
            uid=str(uuid4()),
            name="Склад 1",
            location_type=LocationType.STORE,
            organization=test_organization,
        )
        LocalityWork.objects.create(
            uid=str(uuid4()),
            name="Убрать мусор",
            description="Из контейнеров",
            locality=loc1,
        )
        LocalityWork.objects.create(
            uid=str(uuid4()),
            name="Почистить снег",
            description="На придомовой территории",
            locality=sector1,
        )
        LocalityWork.objects.create(
            uid=str(uuid4()),
            name="Принять роды",
            description="Принять роды у свиноматки",
            locality=sector1,
        )
        breed1 = Breed.objects.create(name="Северокавказская", uid="123456789")
        breed2 = Breed.objects.create(name="Эстонская беконная", uid="223456789")
        breed3 = Breed.objects.create(name="Mиргородская", uid="323456789")
        Breed.objects.create(name="Ландрас", uid="423456789")
        groups = ["Подсосная", "Доращивание", "Откорм"]
        for group in groups:
            Group.objects.create(name=group)
        ReasonForLeaving.objects.create(uid=str(uuid4()), name="Смерть")
        ReasonForLeaving.objects.create(uid=str(uuid4()), name="На мясо")
        ReasonForLeaving.objects.create(uid=str(uuid4()), name="Побег")
        breeds = {
            "me": {"name": breed1.name, "uid": breed1.uid},
            "mom": {"name": breed2.name, "uid": breed2.uid},
            "dad": {"name": breed3.name, "uid": breed3.uid},
        }
        for i in range(10):
            UnitPig.objects.create(
                type=UnitPigType.SOW,
                uid=f"DORY{124 + i}",
                name=f"Имя {i + 1}",
                birthday=date.today() - timedelta(days=10),
                breeds=breeds,
                sector=sector1,
            )
            UnitPig.objects.create(
                type=UnitPigType.SPERM,
                uid=f"SPERM{124 + i}",
                name=f"Имя {i + 1}",
                birthday=date.today() - timedelta(days=10),
                breeds=breeds,
                sector=sector1,
            )
            UnitPig.objects.create(
                type=UnitPigType.BOAR,
                uid=f"BOAR{124 + i}",
                name=f"Имя {i + 1}",
                birthday=date.today() - timedelta(days=10),
                breeds=breeds,
                sector=sector2,
            )
            UnitPig.objects.create(
                type=UnitPigType.PART,
                uid=f"PART{124 + i}",
                name=f"Имя {i + 1}",
                birthday=date.today() - timedelta(days=10),
                breeds=breeds,
                sector=sector3,
            )
        nomenclature_type1 = NomenclatureType.objects.create(name="Корм")
        nomenclature_type2 = NomenclatureType.objects.create(name="Медикаменты")
        for i in range(5):
            Nomenclature.objects.create(
                uid=str(uuid4()),
                name=f"Зерно {i+1}",
                unit=Unit.KILOGRAMS,
                traffic_type=TrafficType.ARRIVAL,
                nomenclature_type=nomenclature_type1,
                store=store,
                count=randint(1, 10),
                partner=Partner.objects.first(),
            )
            Nomenclature.objects.create(
                uid=str(uuid4()),
                name=f"Лекарство {i+1}",
                unit=Unit.KILOGRAMS,
                traffic_type=TrafficType.ARRIVAL,
                nomenclature_type=nomenclature_type2,
                store=store,
                count=randint(1, 10),
                partner=Partner.objects.first(),
            )
            InseminationJournal.objects.create(
                date_in=datetime.today().date(),
                batch_semen_id=f"Партия {i+1}",
                boar=UnitPig.objects.get(uid="SPERM124"),
                count_doses=5,
            )
        for unit_pig in UnitPig.objects.filter(
            Q(type=UnitPigType.BOAR) | Q(type=UnitPigType.SOW),
        ):
            EventLog.objects.create(
                event="BUY",
                action_type="ENTRANCE",
                weight=randint(100, 250),
                unit_pig=unit_pig,
                sector=sector1,
                partner=test_organization,
                info={},
                meta={"state": "Состояние", "count": 1},
                comment="Поступление элитной свинины",
            )
        for unit_pig in UnitPig.objects.filter(type=UnitPigType.PART):
            EventLog.objects.create(
                event="REALIZE",
                action_type="DIRECTION",
                weight=randint(200, 350),
                unit_pig=unit_pig,
                sector=sector2,
                partner=test_organization,
                info={},
                meta={
                    "culling": True,
                    "reason_leaving": "На мясо",
                    "count": randint(200, 350),
                },
                comment="Реализовано на мясо",
            )
        for unit_pig in UnitPig.objects.filter(type=UnitPigType.SOW):
            total_piglets = randint(10, 15)
            EventLog.objects.create(
                event="OTHER",
                action_type="FARROW",
                weight=randint(100, 150),
                unit_pig=unit_pig,
                sector=sector2,
                partner=test_organization,
                info={
                    "total_piglets": total_piglets,
                    "live_piglets": total_piglets - 1,
                    "normal_piglets": total_piglets - 2,
                    "weak_piglets": 1,
                    "dead_piglets": 1,
                    "mummy": 0,
                    "boars": total_piglets // 2,
                    "pigs": round(total_piglets / 2),
                    "weight_group": total_piglets * 100,
                },
                meta={
                    "assistance": True,
                    "nipples_count": randint(10, 15),
                    "socket_number": 1,
                },
                comment="Отличный опорос",
            )
            EventLog.objects.create(
                event="OTHER",
                action_type="INSEMINATION",
                weight=randint(100, 150),
                unit_pig=unit_pig,
                sector=sector2,
                partner=test_organization,
                info={"batch_semen": "Партия семени"},
                meta={
                    "inside_womb": True,
                    "count_doses": randint(1, 3),
                    "artificial": True,
                },
                comment="Осеменение",
            )
            EventLog.objects.create(
                event="OTHER",
                action_type="INSEMINATION",
                weight=randint(100, 150),
                unit_pig=unit_pig,
                sector=sector2,
                partner=test_organization,
                info={"batch_semen": "Партия семени"},
                meta={
                    "inside_womb": True,
                    "count_doses": randint(1, 3),
                    "artificial": True,
                },
                comment="Осеменение",
            )
            EventLog.objects.create(
                event="OTHER",
                action_type="INSEMINATION",
                weight=randint(100, 150),
                unit_pig=unit_pig,
                sector=sector2,
                partner=test_organization,
                info={"batch_semen": "Партия семени"},
                meta={
                    "inside_womb": True,
                    "count_doses": randint(1, 3),
                    "artificial": True,
                },
                comment="Осеменение",
            )
