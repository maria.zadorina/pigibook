from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from apps.food.models import Nomenclature
from apps.food.models import NomenclatureType


@admin.register(NomenclatureType)
class NomenclatureTypeAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "updated",
        "created",
    )
    readonly_fields = ("updated", "created")
    search_fields = ("name",)
    show_full_result_count = False


@admin.register(Nomenclature)
class NomenclatureAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "unit",
        "count",
        "traffic_type",
        "nomenclature_type_link",
        "parent_link",
        "store_link",
        "partner_link",
        "updated",
        "created",
    )
    list_filter = (
        "unit",
        "traffic_type",
        "nomenclature_type",
        "partner",
        "updated",
        "created",
    )
    list_select_related = ("nomenclature_type", "parent", "store", "partner")
    raw_id_fields = ("nomenclature_type", "parent", "store", "partner")
    readonly_fields = ("updated", "created")
    search_fields = ("name",)
    show_full_result_count = False

    def nomenclature_type_link(self, obj):
        if obj.nomenclature_type is None:
            return format_html('<a href="#">none</a>')
        link = reverse(
            "admin:food_nomenclaturetype_change",
            args=[obj.nomenclature_type_id],
        )
        return format_html('<a href="{}">{}</a>', link, obj.nomenclature_type.name)

    nomenclature_type_link.short_description = "Вид номенклатуры"

    def parent_link(self, obj):
        if obj.parent is None:
            return format_html('<a href="#">none</a>')
        link = reverse("admin:food_nomenclature_change", args=[obj.parent_id])
        return format_html('<a href="{}">{}</a>', link, obj.parent.name)

    parent_link.short_description = "Родитель"

    def store_link(self, obj):
        if obj.store is None:
            return format_html('<a href="#">none</a>')
        link = reverse("admin:location_locality_change", args=[obj.store_id])
        return format_html('<a href="{}">{}</a>', link, obj.store.name)

    store_link.short_description = "Склад"

    def partner_link(self, obj):
        if obj.partner is None:
            return format_html('<a href="#">none</a>')
        link = reverse("admin:location_partner_change", args=[obj.partner_id])
        return format_html('<a href="{}">{}</a>', link, obj.partner.name)

    partner_link.short_description = "Поставщик/Получатель"
