from django.db import models


class NomenclatureType(models.Model):
    # Вид номенклатуры
    name = models.CharField("Вид номенклатуры", max_length=256)

    updated = models.DateTimeField("Обновлено", auto_now=True)
    created = models.DateTimeField("Создано", auto_now_add=True)

    class Meta:
        verbose_name = "Вид номенклатуры"
        verbose_name_plural = "Виды номенклатуры"

    def __str__(self):
        return self.name


class TrafficType(models.TextChoices):
    # Вид движения
    NONE = "NONE", "Не определено"
    ARRIVAL = "ARRIVAL", "Приход"
    EXPENSE = "EXPENSE", "Расход"


class Unit(models.TextChoices):
    # Единица измерения
    NONE = "NONE", "Не определено"
    GRAMS = "GRAMS", "Граммы"
    KILOGRAMS = "KILOGRAMS", "Килограммы"
    THINGS = "THINGS", "Штук"


class Nomenclature(models.Model):
    # Номенклатура
    uid = models.CharField("Идентификатор номенклатуры", unique=True, max_length=36)
    name = models.CharField("Номенклатура", max_length=256, help_text="name | Название")
    unit = models.CharField(
        "Единица измерения",
        max_length=32,
        default=Unit.NONE,
        choices=Unit.choices,
        help_text="unit | Единица измерения",
    )
    count = models.PositiveSmallIntegerField(
        "Количество",
        default=0,
        null=True,
        blank=True,
        help_text="count | Количество",
    )
    traffic_type = models.CharField(
        "Вид движения",
        max_length=32,
        default=TrafficType.NONE,
        choices=TrafficType.choices,
        help_text="traffic_type | Вид движения",
    )
    nomenclature_type = models.ForeignKey(
        "NomenclatureType",
        models.SET_NULL,
        null=True,
        blank=True,
        related_name="nomenclature",
        verbose_name="Вид номенклатуры",
        help_text="nomenclature_type | Вид номенклатуры",
    )
    parent = models.ForeignKey(
        "self",
        models.SET_NULL,
        verbose_name="Родитель",
        null=True,
        blank=True,
        db_column="parentid",
        related_name="children",
        help_text="parent | Родитель",
    )
    store = models.ForeignKey(
        "location.Locality",
        models.SET_NULL,
        null=True,
        blank=True,
        related_name="nomenclature",
        verbose_name="Склад",
        help_text="store | Склад | Склад отправитель | Склад получатель",
    )
    partner = models.ForeignKey(
        "location.Partner",
        models.SET_NULL,
        null=True,
        blank=True,
        related_name="nomenclature",
        verbose_name="Поставщик",
        help_text="partner | Поставщик",
    )

    updated = models.DateTimeField("Обновлено", auto_now=True)
    created = models.DateTimeField("Создано", auto_now_add=True)

    class Meta:
        verbose_name = "Номенклатура"
        verbose_name_plural = "Номенклатуры"

    def __str__(self):
        return self.name
