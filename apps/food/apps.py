from django.apps import AppConfig


class FoodConfig(AppConfig):
    name = "apps.food"
    verbose_name = "Корм"
