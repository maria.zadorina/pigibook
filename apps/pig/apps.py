from django.apps import AppConfig


class PigConfig(AppConfig):
    name = "apps.pig"
    verbose_name = "Свиноединицы"
