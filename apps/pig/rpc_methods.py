import logging

from django.db.models import Q
from jsonrpc.decorators import remote_procedure
from jsonrpc.exceptions import InvalidParams

from apps.action.models import ActionType
from apps.action.models import EventLog
from apps.location.models import Locality
from apps.pig.models import UnitPig

logger = logging.getLogger(__name__)


@remote_procedure
def get_pig(pig_id: str = None) -> list:
    """
    Получение свиньи по uid. Если не указан uid - список всех свиней
    Example:
     {
        "jsonrpc": "2.0",
        "id": 123,
        "method": "get_pig",
        "params": {
            "uid_pig": "DORY124"
        }
    }
    """
    query_lookups = {}
    if pig_id:
        query_lookups.update(uid=pig_id)
    pigs = list(
        UnitPig.objects.select_related("sector")
        .filter(**query_lookups)
        .values(
            "uid",
            "name",
            "birthday",
            "breeds",
            "sector__id",
            "sector__name",
        ),
    )
    for pig in pigs:
        pig["birthday"] = pig.get("birthday").strftime("%Y-%m-%d")
        pig["breeds"] = pig.get("breeds")
    return pigs


@remote_procedure
def get_pig_location(pig_id: str = None) -> list:
    """
    Получение локации свиньи по uid. Если не указан uid - список всех локаций свиней
    Example:
     {
        "jsonrpc": "2.0",
        "id": 123,
        "method": "get_pig",
        "params": {
            "uid_pig": "DORY124"
        }
    }
    """
    query_lookups = {}
    if pig_id:
        query_lookups.update(uid=pig_id)
    pigs = list(
        UnitPig.objects.select_related("sector")
        .filter(**query_lookups)
        .values(
            "uid",
            "sector__id",
            "sector__name",
        ),
    )
    return pigs


@remote_procedure
def create_unitpig(
    type: str,
    uid: str,
    name: str,
    birthday: str,
    breeds: dict,
    sector_id: int,
) -> dict:
    try:
        sector = Locality.objects.get(id=sector_id)
    except Locality.DoesNotExist:
        sector = None
    obj, created = UnitPig.objects.update_or_create(
        type=type,
        uid=uid,
        defaults={
            "name": name,
            "birthday": birthday,
            "breeds": breeds,
            "sector": sector,
        },
    )
    return {"unit_pig_uid": obj.uid, "created": created}


@remote_procedure
def get_history_changes(sow_id: str) -> list:
    events = list(
        EventLog.objects.filter(unit_pig__uid=sow_id, sector__isnull=False).values(
            "event",
            "action_type",
            "sector__id",
            "sector__name",
            "info",
            "meta",
            "created",
        ),
    )
    for event in events:
        event["created"] = event.get("created").strftime("%Y-%m-%dT%H:%M:%S")
    return events


# todo: 1. тип движения (откуда куда)
#  2. мертворожденных
#  3. осеменение то кем


@remote_procedure
def get_insemination_history(sow_id: str) -> list:
    events = list(
        EventLog.objects.filter(
            unit_pig__uid=sow_id,
            action_type=ActionType.INSEMINATION,
            sector__isnull=False,
        ).values("sector__id", "sector__name", "info", "meta", "created"),
    )
    for event in events:
        event["created"] = event.get("created").strftime("%Y-%m-%dT%H:%M:%S")
    return events


@remote_procedure
def get_sow_card(sow_id: str) -> dict:
    try:
        sow = UnitPig.objects.get(uid=sow_id)
        breed_me = sow.breeds.get("me")
    except UnitPig.DoesNotExist:
        raise InvalidParams(f"Sow {sow_id} does not exist")
    insemination = EventLog.objects.filter(
        unit_pig=sow,
        action_type=ActionType.INSEMINATION,
    ).last()
    qs_farrows = EventLog.objects.filter(unit_pig=sow, action_type=ActionType.FARROW)
    farrow_count = qs_farrows.count()
    farrow_last = qs_farrows.last()
    result = {
        "uid": sow.uid,
        "birthday": sow.birthday.strftime("%d.%m.%Y"),
        "breed_me_uid": breed_me.get("uid") if breed_me else "",
        "breed_me_name": breed_me.get("name") if breed_me else "",
        "sector_id": sow.sector.id if sow.sector else "",
        "sector_name": sow.sector.name if sow.sector else "",
        "expected_farrow": insemination.expected_farrow() if insemination else "",
        "insemination_week": insemination.insemination_week() if insemination else 0,
        "farrow_count": farrow_count,
        "nipples_count": farrow_last.meta.get("nipples_count") if farrow_last else 0,
    }
    return result


# todo:
#  1. Отец, Мать
#  2. Интервал между опоросами
