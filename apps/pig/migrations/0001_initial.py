# Generated by Django 4.1.5 on 2023-02-06 11:27

from django.db import migrations, models
import django.db.models.deletion
import jsonfield.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('location', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Breed',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256, verbose_name='Название породы')),
                ('uid', models.CharField(max_length=36, unique=True, verbose_name='Идентификатор породы')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обновлено')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Создано')),
            ],
            options={
                'verbose_name': 'Порода',
                'verbose_name_plural': 'Породы',
            },
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='Подсосная, доращивание, откорм', max_length=256, verbose_name='Группа животного')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обновлено')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Создано')),
            ],
            options={
                'verbose_name': 'Группа животного',
                'verbose_name_plural': 'Группы животных',
            },
        ),
        migrations.CreateModel(
            name='ReasonForLeaving',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256, verbose_name='Причина выбытия')),
                ('uid', models.CharField(max_length=36, unique=True, verbose_name='Идентификатор причины выбытия')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обновлено')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Создано')),
            ],
            options={
                'verbose_name': 'Причина выбытия',
                'verbose_name_plural': 'Причины выбытия',
            },
        ),
        migrations.CreateModel(
            name='UnitPig',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(choices=[('NONE', 'Не определено'), ('SOW', 'Свиноматка'), ('SPERM', 'Сперма'), ('BOAR', 'Хряк'), ('PART', 'Партия безномерных')], default='NONE', help_text='type | Тип свино-единицы', max_length=15, verbose_name='Тип свино-единицы')),
                ('uid', models.CharField(max_length=36, unique=True, verbose_name='Идентификатор свино-единицы')),
                ('name', models.CharField(blank=True, max_length=256, null=True, verbose_name='Имя свино-единицы')),
                ('birthday', models.DateField(blank=True, null=True, verbose_name='Дата рождения')),
                ('implemented', models.BooleanField(default=False, help_text='implemented | Фланг позволяет понять, реализована свиноединица или нет', verbose_name='Реализовано')),
                ('breeds', jsonfield.fields.JSONField(default={}, help_text='breeds | Порода свиньи, мамы и папы | {"me": {"name": "Северокавказская", "uid": "123456789"}, "mom": {"name": "Эстонская беконная", "uid": "223456789"}, "dad": {"name": "Mиргородская", "uid": "323456789"}}', verbose_name='Породы')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обновлено')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Создано')),
                ('sector', models.ForeignKey(blank=True, help_text='sector | Сектор', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='units_pig', to='location.locality', verbose_name='Сектор')),
            ],
            options={
                'verbose_name': 'Свиноединица',
                'verbose_name_plural': 'Свиноединицы',
            },
        ),
    ]
