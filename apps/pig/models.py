from django.db import models
from jsonfield import JSONField


class Breed(models.Model):
    # Справочник пород
    # todo: add norms
    name = models.CharField("Название породы", max_length=256)
    uid = models.CharField("Идентификатор породы", unique=True, max_length=36)

    updated = models.DateTimeField("Обновлено", auto_now=True)
    created = models.DateTimeField("Создано", auto_now_add=True)

    class Meta:
        verbose_name = "Порода"
        verbose_name_plural = "Породы"

    def __str__(self):
        return self.name


class Group(models.Model):
    # Группа животного
    name = models.CharField(
        "Группа животного",
        max_length=256,
        help_text="Подсосная, доращивание, откорм",
    )
    # fixme: model replace on choice
    updated = models.DateTimeField("Обновлено", auto_now=True)
    created = models.DateTimeField("Создано", auto_now_add=True)

    class Meta:
        verbose_name = "Группа животного"
        verbose_name_plural = "Группы животных"

    def __str__(self):
        return self.name


# todo: Справочник "Технологические группы"
class UnitPigType(models.TextChoices):
    # Тип свино-единицы
    NONE = "NONE", "Не определено"
    SOW = "SOW", "Свиноматка"
    SPERM = "SPERM", "Сперма"
    BOAR = "BOAR", "Хряк"
    PART = "PART", "Партия безномерных"


class UnitPig(models.Model):
    # Свино единица либо группа безномерных животных - свиномасса либо полноценная свинья, хряк
    type = models.CharField(
        "Тип свино-единицы",
        max_length=15,
        default=UnitPigType.NONE,
        choices=UnitPigType.choices,
        help_text="type | Тип свино-единицы",
    )
    uid = models.CharField("Идентификатор свино-единицы", unique=True, max_length=36)
    name = models.CharField("Имя свино-единицы", max_length=256, null=True, blank=True)
    birthday = models.DateField("Дата рождения", null=True, blank=True)
    implemented = models.BooleanField(
        "Реализовано",
        default=False,
        help_text="implemented | Фланг позволяет понять, реализована свиноединица или нет",
    )
    breeds = JSONField(
        verbose_name="Породы",
        default={},
        help_text="breeds | Порода свиньи, мамы и папы | "
                  '{"me": {"name": "Северокавказская", "uid": "123456789"}, '
                  '"mom": {"name": "Эстонская беконная", "uid": "223456789"}, '
                  '"dad": {"name": "Mиргородская", "uid": "323456789"}}',
    )  # fixme add mom and dad
    sector = models.ForeignKey(
        "location.Locality",
        models.SET_NULL,
        null=True,
        blank=True,  # todo: think about not null
        related_name="units_pig",
        verbose_name="Сектор",
        help_text="sector | Сектор",
    )
    updated = models.DateTimeField("Обновлено", auto_now=True)
    created = models.DateTimeField("Создано", auto_now_add=True)

    class Meta:
        verbose_name = "Свиноединица"
        verbose_name_plural = "Свиноединицы"

    def __str__(self):
        return self.uid

    def update(self, **kwargs):
        """Update unitpig object"""
        for field, value in kwargs.items():
            if hasattr(self, field):
                setattr(self, field, value)
        self.save()


class ReasonForLeaving(models.Model):
    # Причина выбытия
    name = models.CharField("Причина выбытия", max_length=256)
    uid = models.CharField("Идентификатор причины выбытия", unique=True, max_length=36)
    updated = models.DateTimeField("Обновлено", auto_now=True)
    created = models.DateTimeField("Создано", auto_now_add=True)

    class Meta:
        verbose_name = "Причина выбытия"
        verbose_name_plural = "Причины выбытия"

    def __str__(self):
        return self.name
