from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from apps.pig.models import Breed
from apps.pig.models import Group
from apps.pig.models import ReasonForLeaving
from apps.pig.models import UnitPig


@admin.register(Breed)
class BreedAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "updated",
        "created",
    )
    search_fields = ("name",)
    readonly_fields = ("updated", "created")


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "updated",
        "created",
    )
    search_fields = ("name",)
    readonly_fields = ("updated", "created")


@admin.register(UnitPig)
class UnitPigAdmin(admin.ModelAdmin):
    list_display = (
        "type",
        "uid",
        "name",
        "implemented",
        "sector_link",
        "birthday",
        "updated",
        "created",
    )
    list_filter = ("type", "sector")
    list_select_related = ("sector",)
    raw_id_fields = ("sector",)
    search_fields = ("name", "uid")
    readonly_fields = ("updated", "created")
    show_full_result_count = False

    def sector_link(self, obj):
        if obj.sector is None:
            return format_html('<a href="#">none</a>')
        link = reverse("admin:location_locality_change", args=[obj.sector_id])
        return format_html('<a href="{}">{}</a>', link, obj.sector.name)

    sector_link.short_description = "Сектор"


@admin.register(ReasonForLeaving)
class ReasonForLeavingAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "updated",
        "created",
    )
    search_fields = ("name",)
    readonly_fields = ("updated", "created")
