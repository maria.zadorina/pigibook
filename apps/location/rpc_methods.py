import logging

from jsonrpc.decorators import remote_procedure

from .models import Locality
from .models import Partner

log = logging.getLogger(__name__)


@remote_procedure
def get_location(location_id):
    pass


@remote_procedure
def create_location(
    name: str,
    location_type: str,
    organization_id: int = None,
    period: int = None,
    parent_id: int = None,
) -> dict:
    organization, parent = None, None
    try:
        organization = (
            Partner.objects.get(id=organization_id) if organization_id else None
        )
        parent = Locality.objects.get(id=parent_id) if parent_id else None
    except Partner.DoesNotExist:
        log.error(f"Partner {organization_id} does not exist")
    except Locality.DoesNotExist:
        log.error(f"Parent {parent_id} does not exist")

    localty, created = Locality.objects.update_or_create(
        name=name,
        location_type=location_type,
        defaults={
            "organization": organization,
            "period": period,
            "parent": parent,
        },
    )
    return {"localty_id": localty.id, "localty_name": localty.name, "created": created}
