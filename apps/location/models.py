from django.db import models


class LocationType(models.TextChoices):
    # Тип локации
    NONE = "NONE", "Не определено"
    STORE = "STORE", "Склад"
    PIG_COMPLEX = "PIG_COMPLEX", "Свиноводческий комплекс"
    DISTRICT = "DISTRICT", "Участок"
    SECTOR = "SECTOR", "Сектор"


class Locality(models.Model):
    """Локации: Свинокомплекс, Участок, Сектор, Склад"""

    uid = models.CharField("Идентификатор локации", unique=True, max_length=36)
    name = models.CharField(
        "Название локации (свинокоплекса / участка / сектора / склада)",
        max_length=256,
        help_text="name | Название локации | Иерархическая структура таблицы",
    )
    location_type = models.CharField(
        "Тип локации",
        max_length=15,
        default=LocationType.NONE,
        choices=LocationType.choices,
        help_text="location_type | Тип локации",
    )
    period = models.PositiveSmallIntegerField(
        "Период содержания в секторе",
        default=None,
        null=True,
        blank=True,
        help_text="period | Период",
    )
    organization = models.ForeignKey(
        "Partner",
        models.SET_NULL,
        null=True,
        blank=True,
        related_name="localities",
        verbose_name="Организация",
        help_text="organization | Юридическое лицо",
    )
    parent = models.ForeignKey(
        "self",
        models.SET_NULL,
        verbose_name="Участок",
        null=True,
        blank=True,
        db_column="parentid",
        related_name="children",
        help_text="parent | Участок (родителем может быть только участок)",
    )

    updated = models.DateTimeField("Обновлено", auto_now=True)
    created = models.DateTimeField("Создано", auto_now_add=True)

    class Meta:
        verbose_name = "Локация"
        verbose_name_plural = "Локации"

    def __str__(self):
        return self.name


class ReceiptType(models.TextChoices):
    # Тип получения
    NONE = "NONE", "Не определено"
    EXTERNAL = "EXTERNAL", "Внешнее (стороннее)"
    INTERNAL = "INTERNAL", "Внутреннее"


class Partner(models.Model):
    # Контрагент/Заказчик
    uid = models.CharField(
        "Идентификатор контрагента/заказчика",
        unique=True,
        max_length=36,
    )
    name = models.CharField("Название контрагента/заказчика", max_length=256)
    inn = models.CharField(
        "ИНН",
        max_length=64,
        null=True,
        blank=True,
        help_text="inn | ИНН",
    )
    receipt_type = models.CharField(
        "Тип поступления",
        max_length=32,
        default=ReceiptType.NONE,
        choices=ReceiptType.choices,
        help_text="receipt_type | Тип поступления",
    )

    updated = models.DateTimeField("Обновлено", auto_now=True)
    created = models.DateTimeField("Создано", auto_now_add=True)

    class Meta:
        verbose_name = "Контрагент/Заказчик"
        verbose_name_plural = "Контрагенты/Заказчики"

    def __str__(self):
        return self.name


class LocalityWork(models.Model):
    # Работы на участке
    uid = models.CharField(
        "Идентификатор работы на участке",
        unique=True,
        max_length=36,
    )
    name = models.CharField("Наименование работы на участке", max_length=256)
    description = models.CharField(
        "Описание",
        max_length=1024,
        null=True,
        blank=True,
        help_text="description | Описание",
    )
    locality = models.ForeignKey(
        "Locality",
        models.SET_NULL,
        null=True,
        blank=True,
        related_name="locality_works",
        verbose_name="Участок",
        help_text="locality | Участок",
    )

    updated = models.DateTimeField("Обновлено", auto_now=True)
    created = models.DateTimeField("Создано", auto_now_add=True)

    class Meta:
        verbose_name = "Работа на участке"
        verbose_name_plural = "Работы на участке"

    def __str__(self):
        return self.name
