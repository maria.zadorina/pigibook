from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from apps.location.models import Locality
from apps.location.models import LocalityWork
from apps.location.models import Partner


@admin.register(Partner)
class PartnerAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "inn",
        "receipt_type",
        "updated",
        "created",
    )
    list_filter = ("receipt_type",)
    search_fields = ("name",)
    readonly_fields = ("updated", "created")


@admin.register(Locality)
class LocalityAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "period",
        "parent_link",
        "location_type",
        "organization_link",
        "updated",
        "created",
    )
    list_filter = ("parent", "location_type")
    search_fields = ("name",)
    list_select_related = ("parent", "organization")
    raw_id_fields = ("parent", "organization")
    readonly_fields = ("updated", "created")
    show_full_result_count = False

    def parent_link(self, obj):
        if obj.parent is None:
            return format_html('<a href="#">none</a>')
        link = reverse("admin:location_locality_change", args=[obj.parent_id])
        return format_html('<a href="{}">{}</a>', link, obj.parent.name)

    parent_link.short_description = "Родитель"

    def organization_link(self, obj):
        if obj.organization is None:
            return format_html('<a href="#">none</a>')
        link = reverse("admin:location_partner_change", args=[obj.organization_id])
        return format_html('<a href="{}">{}</a>', link, obj.organization.name)

    organization_link.short_description = "Поставщик/Получатель"


@admin.register(LocalityWork)
class LocalityWorkAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "description",
        "locality",
        "updated",
        "created",
    )
    list_filter = ("locality",)
    search_fields = ("name",)
    list_select_related = ("locality",)
    raw_id_fields = ("locality",)
    readonly_fields = ("updated", "created")
