from django.apps import AppConfig


class InseminationConfig(AppConfig):
    name = "apps.insemination"
    verbose_name = "Осеменение"
