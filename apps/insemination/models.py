from django.db import models


# fixme: сперма это тоже тип свиноединицы, поэтому можно все вынести в таблицу action
#  хотя у одной свиньи может быть несколько партий семени
class InseminationJournal(models.Model):
    date_in = models.DateField("Дата поступления", null=True, blank=True)
    batch_semen_id = models.CharField("Номер партии семени", max_length=256)
    boar = models.ForeignKey(
        "pig.UnitPig",
        models.SET_NULL,
        null=True,
        blank=True,
        related_name="insemination",
        verbose_name="Хряк",
        help_text="boar | Хряк",
    )
    count_doses = models.IntegerField("Количество доз, шт", null=True, blank=True)
    OP_day_null = models.IntegerField(
        "ОП при поступлении",
        null=True,
        blank=True,
        help_text="OP_day_null | Общая подвижность при поступлении",
    )
    PP_day_null = models.IntegerField(
        "ПП при поступлении",
        null=True,
        blank=True,
        help_text="PP_day_null | Прямолинейная подвижность при поступлении",
    )

    OP_day_first = models.IntegerField(
        "ОП через 1 сутки",
        null=True,
        blank=True,
        help_text="OP_day_first | Общая подвижность через 1 сутки",
    )
    PP_day_first = models.IntegerField(
        "ПП через 1 сутки",
        null=True,
        blank=True,
        help_text="PP_day_first | Прямолинейная подвижность через 1 сутки",
    )
    # todo what is it
    code_insemination_first = models.CharField(
        "Код осем. через 1 сутки",
        max_length=256,
        null=True,
        blank=True,
    )

    OP_day_second = models.IntegerField(
        "ОП через 2 суток",
        null=True,
        blank=True,
        help_text="OP_day_second | Общая подвижность через 2 сутки",
    )
    PP_day_second = models.IntegerField(
        "ПП через 2 суток",
        null=True,
        blank=True,
        help_text="PP_day_second | Прямолинейная подвижность через 2 сутки",
    )
    # todo what is it
    code_insemination_second = models.CharField(
        "Код осем. через 2 суток",
        max_length=256,
        null=True,
        blank=True,
    )

    OP_day_third = models.IntegerField(
        "ОП через 3 суток",
        null=True,
        blank=True,
        help_text="OP_day_third | Общая подвижность через 3 суток",
    )
    PP_day_third = models.IntegerField(
        "ПП через 3 суток",
        null=True,
        blank=True,
        help_text="PP_day_third | Прямолинейная подвижность через 3 суток",
    )
    # todo what is it
    code_insemination_third = models.CharField(
        "Код осем. через 3 суток",
        max_length=256,
        null=True,
        blank=True,
    )

    OP_day_fourth = models.IntegerField(
        "ОП через 4 суток",
        null=True,
        blank=True,
        help_text="OP_day_fourth | Общая подвижность через 4 суток",
    )
    PP_day_fourth = models.IntegerField(
        "ПП через 4 суток",
        null=True,
        blank=True,
        help_text="PP_day_fourth | Прямолинейная подвижность через 4 суток",
    )
    # todo what is it
    code_insemination_fourth = models.CharField(
        "Код осем. через 4 суток",
        max_length=256,
        null=True,
        blank=True,
    )

    is_deleted = models.IntegerField("Утилизировано, доз", null=True, blank=True)
    comment = models.TextField(
        "Комментарий",
        blank=True,
        help_text="comment",
        null=True,
    )

    updated = models.DateTimeField("Обновлено", auto_now=True)
    created = models.DateTimeField("Создано", auto_now_add=True)

    class Meta:
        verbose_name = "Журнал спермы"
        verbose_name_plural = "Журнал спермы"

    def __str__(self):
        return f"Образец спермы {self.batch_semen_id}"
