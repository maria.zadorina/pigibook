from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from apps.insemination.models import InseminationJournal


@admin.register(InseminationJournal)
class InseminationJournalAdmin(admin.ModelAdmin):
    list_display = ("date_in", "boar_link", "count_doses", "updated", "created")
    list_select_related = ("boar",)
    raw_id_fields = ("boar",)
    readonly_fields = ("updated", "created")
    show_full_result_count = False

    def boar_link(self, obj):
        if obj.boar is None:
            return format_html('<a href="#">none</a>')
        link = reverse("admin:pig_unitpig_change", args=[obj.boar_id])
        return format_html('<a href="{}">{}</a>', link, obj.boar.uid)

    boar_link.short_description = "Свино-единица"
